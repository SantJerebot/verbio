/*
 * Parser.h
 *
 *  Created on: 10 de jul. 2020
 *      Author: p.sivecas
 */

#include <string>
#include <vector>

class SearchNode;

class NumberParser
{
	public:

	/**
	 * Reads a string and returns the same string but replacing written numbers by numerical ones.
	 *
	 * @param iSn SearchNode tree structure containing the numbers to search for
	 * @param iText Text where the numbers will be replaced
	 * @return a copy of iText with the written numbers replaced
	 */
		std::string parse(const SearchNode* iSn, const std::string& iText);
};


