/*
 * NumberCluster.h
 *
 *  Created on: 11 de jul. 2020
 *      Author: p.sivecas
 */

#pragma once

#include <string>
#include <memory>
#include <array>
#include <vector>

/*
 * This enum allows us to know the kind of chunk we are dealing with. The value of the enum is important,
 * as the logic to link chunks together uses its relative value to know if they can be clustered together or not.
 */
enum class NumberChunkType : unsigned int
{
	invalid = 0,
	digit = 1,         // 1 - 9
	two_digits    = 2, // 12, 20, 30, 40 .. 90
	hundred   = 3,     // 100, 1000, 1 000 000, 1 000 000 000
	thousand = 4,
	million = 5,
	billion = 6
};
constexpr unsigned int NumberChunkType_size = 8;

enum class LinkResult : char
{
	linked = 'l',		// Chunk was linked successfully
	standalone = 's',   // Chunk was not linked because it is part of a new cluster
	invalid = 'i',      // Chunk was not linked because it is not a valid chunk and should be discarded
	AND = 'a'           // Chunk hasAnd flag was set to true, AND chunks should never be linked
};

/*
 * This class represents a match for a specific number in a string. It contains logic that allows it
 * to know if it can be linked together with other chunks, forming a longer number.
 */
class NumberChunk
{
	protected:
		// Value of the chunk, used to compute the number of the cluster
		unsigned int value;
		// Link to the next chunk in the cluster. This is no longer used (except for printing) and should be removed.
		std::shared_ptr<NumberChunk> nextChunk;
		NumberChunkType type;
		// True if the chunk represents a year
		bool isYear;
		// True if the chunk is followed by an AND
		bool hasAnd;
		// Contains the value of the largest chunk we have seen while processing a cluster
		int maxLinkedValue;

	public:
		static constexpr unsigned int AND = 101;

		// Global variable to configure if we want to distinguish years. It should NOT be a global variable. Lack of time :)
		static bool PICK_UP_YEARS;

		NumberChunk();
		NumberChunk(const NumberChunk& iNumChunk);
		NumberChunk(const unsigned int iValue, const NumberChunkType iType);

		unsigned int getValue() const;
		const std::shared_ptr<NumberChunk> getNextChunk() const;
		NumberChunkType getType() const;
		bool getHasAnd() const;
		void setHasAnd(const bool iHasAnd);
		bool getIsYear() const;
		void setIsYear(const bool iIsYear);
		unsigned int getMaxLinkedValue() const;
		void setMaxLinkedValue(const unsigned int iValue);
		void setType(const NumberChunkType iType);
		std::string toString() const;
		void setNextChunk(const std::shared_ptr<NumberChunk> iPrevChunk);
};

using NumberChunkPtr = std::shared_ptr<NumberChunk>;
using NumberChunkTypeMask = std::array<bool, NumberChunkType_size>;

/**
 * Checks if two chunks can be linked together (ie are part of the same number).
 *
 * @param iCurrentChunk Chunk that we are currently looking at
 * @param ioPreviousChunk Chunk that we have already looked at and it might be linked (both chunks are separated by a space).
 * @return LinkResult telling if chunks can be linked
 */
LinkResult linkCurrentToPreviousChunk(NumberChunkPtr iCurrentChunk,
									  NumberChunkPtr ioPreviousChunk,
									  NumberChunkTypeMask& ioUsedTypes);

/**
 * Given a cluster of chunks, computes the value of the number they form.
 *
 * @param iCluster vector of chunks that form a number
 * @return value of the number represented by the cluster
 */
unsigned int computeNumber(const std::vector<NumberChunkPtr>& iCluster);
