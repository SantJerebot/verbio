/*
 * NumberChunk.cpp
 *
 *  Created on: 11 de jul. 2020
 *      Author: p.sivecas
 */

#include "NumberChunk.h"
#include "iostream"

using namespace std;

bool NumberChunk::PICK_UP_YEARS = false;

NumberChunk::NumberChunk() :
		value(0), type(NumberChunkType::invalid), hasAnd(false), isYear(false), maxLinkedValue(0)
{}

NumberChunk::NumberChunk(const NumberChunk& iNumChunk) :
		value(iNumChunk.getValue()), type(iNumChunk.getType()), hasAnd(iNumChunk.getHasAnd()), isYear(iNumChunk.getIsYear()),
		maxLinkedValue(iNumChunk.getMaxLinkedValue())
{}

NumberChunk::NumberChunk(const unsigned int iValue, const NumberChunkType iType) :
		value(iValue), type(iType), hasAnd(false), isYear(false), maxLinkedValue(iValue)
{}

NumberChunkType NumberChunk::getType() const
{
	return type;
}

unsigned int NumberChunk::getValue() const
{
	return value;
}

bool NumberChunk::getHasAnd() const
{
	return hasAnd;
}

void NumberChunk::setHasAnd(const bool iHasAnd)
{
	hasAnd = iHasAnd;
}

bool NumberChunk::getIsYear() const
{
	return isYear;
}

void NumberChunk::setIsYear(const bool iIsYear)
{
	isYear = iIsYear;
}

unsigned int NumberChunk::getMaxLinkedValue() const
{
	return maxLinkedValue;
}

void NumberChunk::setMaxLinkedValue(const unsigned int iValue)
{
	maxLinkedValue = iValue;
}

const NumberChunkPtr NumberChunk::getNextChunk() const
{
	return nextChunk;
}

void NumberChunk::setNextChunk(const shared_ptr<NumberChunk> iNextChunk)
{
	nextChunk = iNextChunk;
}

void NumberChunk::setType(const NumberChunkType iType)
{
	type = iType;
}

string NumberChunk::toString() const
{
	if(getNextChunk())
	{
		return std::to_string(value) + "(" + std::to_string(static_cast<unsigned int>(type)) + ")->" + getNextChunk()->toString();
	}
	else
	{
		return std::to_string(value) + "(" + std::to_string(static_cast<unsigned int>(type)) + ")";
	}
}

LinkResult linkCurrentToPreviousChunk(NumberChunkPtr iCurrentChunk,
									  NumberChunkPtr ioPrevChunk,
									  NumberChunkTypeMask& ioUsedTypes)
{
	//cout << "Trying to link " << iCurrentChunk->toString() << " to " << (ioPrevChunk == nullptr ? "nullptr" : ioPrevChunk->toString()) << endl;
	LinkResult aResult = LinkResult::invalid;
	const NumberChunkType aPrevType = ioPrevChunk == nullptr ? NumberChunkType::invalid : ioPrevChunk->getType();
	const NumberChunkType aCurrentType = iCurrentChunk->getType();
	const unsigned int aCurrentTypeIdx = static_cast<unsigned int>(aCurrentType);

	if(ioPrevChunk == nullptr ||
	   ioUsedTypes[aCurrentTypeIdx] ||
	   ioPrevChunk->getIsYear())
	{
		aResult = LinkResult::standalone;
	}
	else if(aCurrentType == NumberChunkType::digit ||
			aCurrentType == NumberChunkType::two_digits)
	{
		if(aPrevType == NumberChunkType::digit ||
		   aPrevType == NumberChunkType::two_digits)
		{
			if(NumberChunk::PICK_UP_YEARS &&
			   (aCurrentType == NumberChunkType::two_digits))
			{
				iCurrentChunk->setIsYear(true);
				aResult = LinkResult::linked;
			}
			else
			{
				aResult = LinkResult::standalone;
			}
		}
		else
		{
			aResult = LinkResult::linked;
		}

	}
	else if(aCurrentType == NumberChunkType::invalid)
	{
		if(iCurrentChunk->getValue() ==  NumberChunk::AND)
		{
			if(aPrevType == NumberChunkType::digit ||
			   aPrevType == NumberChunkType::two_digits)
			{
				aResult = LinkResult::invalid;
			}
			else
			{
				ioPrevChunk->setHasAnd(true);
				aResult = LinkResult::AND;
			}
		}
	}
	else if(aCurrentType == NumberChunkType::million)
	{
		if(aPrevType < aCurrentType && ioPrevChunk->getMaxLinkedValue() > 999)
		{
			aResult = LinkResult::standalone; //Exercise is only up to one billion, we should store much bigger numbers otherwise
		}
		else
		{
			aResult = LinkResult::linked;
		}
	}
	else if(aCurrentType == NumberChunkType::billion)
	{
		if(ioPrevChunk->getMaxLinkedValue() > 1)
		{
			aResult = LinkResult::standalone; //Exercise is only up to one billion, we should store much bigger numbers otherwise
		}
		else
		{
			aResult = LinkResult::linked;
		}
	}
	else if(aCurrentType != aPrevType)
	{
		if(aCurrentType > aPrevType && ioPrevChunk->getHasAnd())
		{
			aResult = LinkResult::standalone;
		}
		else
		{
			if((aPrevType == NumberChunkType::two_digits) &&
				aCurrentType == NumberChunkType::hundred)
			{
				iCurrentChunk->setType(NumberChunkType::thousand); //Consider fifteen hundred, etc.
			}
			aResult =  LinkResult::linked;
		}
	}
	else
	{
		aResult = LinkResult::standalone;
	}

	if(aResult == LinkResult::standalone)
	{
		ioUsedTypes.fill(false);
	}
	else if(aResult == LinkResult::linked)
	{
		if(ioPrevChunk->getMaxLinkedValue() > iCurrentChunk->getMaxLinkedValue())
		{
			iCurrentChunk->setMaxLinkedValue(ioPrevChunk->getMaxLinkedValue());
		}
		ioPrevChunk->setNextChunk(iCurrentChunk);
		fill_n(ioUsedTypes.begin(), aCurrentTypeIdx, false);
	}

	ioUsedTypes[aCurrentTypeIdx] = true;

	//cout << "Result of link is:" << static_cast<char>(aResult) << endl;
	return aResult;
}

unsigned int maxValueIdx(const vector<NumberChunkPtr> iCluster, const unsigned int aIdxStart, const unsigned int aIdxEnd)
{
	unsigned int maxValueIdx = aIdxStart;
	for(int aIdx = aIdxStart; aIdx <= aIdxEnd; aIdx++)
	{
		if(iCluster[aIdx]->getType() > iCluster[maxValueIdx]->getType())
		{
			maxValueIdx = aIdx;
		}
	}
	return maxValueIdx;
}

unsigned int computeNumberRec(const vector<NumberChunkPtr> iCluster, const unsigned int aIdxStart, const unsigned int aIdxEnd)
{
	if(aIdxStart == aIdxEnd)
	{
		return iCluster[aIdxStart]->getValue();
	}

	const unsigned int aMaxValueIdx = maxValueIdx(iCluster, aIdxStart, aIdxEnd);

	unsigned int aMulValue = 1;
	if(aMaxValueIdx > aIdxStart)
	{
		aMulValue = computeNumberRec(iCluster, aIdxStart, aMaxValueIdx-1);
	}

	unsigned int aSumValue = 0;
	if(aMaxValueIdx < aIdxEnd)
	{
		aSumValue = computeNumberRec(iCluster, aMaxValueIdx+1, aIdxEnd);
	}
	return (iCluster[aMaxValueIdx]->getValue() * aMulValue) + aSumValue;
}

unsigned int computeNumber(const vector<NumberChunkPtr>& iCluster)
{
	if(iCluster.empty())
	{
		return 0;
	}
	if(NumberChunk::PICK_UP_YEARS)
	{
		if(iCluster.size() == 2 && iCluster[1]->getIsYear())
		{
			return (iCluster[0]->getValue() * 100) + iCluster[1]->getValue();
		}
	}
	const unsigned int aMaxValueIdx = maxValueIdx(iCluster, 0, iCluster.size()-1);
	unsigned int aMulValue = 1;
	if(aMaxValueIdx > 0)
	{
		aMulValue = computeNumberRec(iCluster, 0, aMaxValueIdx-1);
	}

	unsigned int aSumValue = 0;
	if(aMaxValueIdx < iCluster.size()-1)
	{
		aSumValue = computeNumberRec(iCluster, aMaxValueIdx+1, iCluster.size()-1);
	}
	return (iCluster[aMaxValueIdx]->getValue() * aMulValue) + aSumValue;
}

