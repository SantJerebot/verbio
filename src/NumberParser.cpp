/*
 * Parser.cpp
 *
 *  Created on: 10 de jul. 2020
 *      Author: p.sivecas
 */

#include "NumberParser.h"
#include "SearchNode/SearchNode.h"
#include <fstream>

using namespace std;


inline void moveClusterToResult(vector<NumberChunkPtr>& iCluster, string& oResult, NumberChunkTypeMask& oUsedTypes)
{
	if(!iCluster.empty())
	{
		oResult += std::to_string(computeNumber(iCluster));
		iCluster.clear();
	}
	oUsedTypes.fill(false);
}

/**
 * Puts a chunk into a cluster. In order to do that it checks wether it has to be put linked or not,
 * if buffers of characters from the original string need to be copied to the new one, etc.
 */
inline void processChunk(const SearchNode* iSnWithValue,
						 bool& ioHasToLinkToPrevious,
						 bool iCanLinkToNext,
						 NumberChunkPtr& iPreviousChunk,
						 std::string& iPreviousDelimiter,
						 NumberChunkTypeMask& iUsedTypes,
						 vector<NumberChunkPtr>& ioCluster,
						 string& ioResultStr,
						 string& ioTmpBuffer)
{
	NumberChunkPtr aChunk = make_shared<NumberChunk>(iSnWithValue->getChunk());
	LinkResult aLinked = LinkResult::standalone;
	if(ioHasToLinkToPrevious)
	{
		//cout << "ioHasToLinkToPrevious" << endl;
		aLinked = linkCurrentToPreviousChunk(aChunk, iPreviousChunk, iUsedTypes);

		if(aChunk->getValue() == NumberChunk::AND)
		{
			//cout << "we have an AND" << endl;
			if(!iCanLinkToNext || aLinked == LinkResult::invalid)
			{
				aLinked = LinkResult::invalid;
				moveClusterToResult(ioCluster, ioResultStr, iUsedTypes);
				ioResultStr += ioTmpBuffer;
				ioTmpBuffer.clear();
			}
		}
		else
		{
			//cout << "we have regular chunk" << endl;
			if(aLinked == LinkResult::standalone)
			{
				moveClusterToResult(ioCluster, ioResultStr, iUsedTypes);
				if(iPreviousChunk->getHasAnd())
				{
					ioResultStr += ioTmpBuffer.substr(0, 4);
				}
				ioResultStr += ' ';
			}
			else if(aLinked == LinkResult::AND)
			{

			}
			//cout << iPreviousChunk->toString() << endl;
			ioTmpBuffer.clear();
		}
	}
	else
	{
		if(aChunk->getValue() == NumberChunk::AND)
		{
			//cout << "we have an AND, no linking" << endl;
			aLinked == LinkResult::invalid;
			ioResultStr += ioTmpBuffer;
			ioTmpBuffer.clear();
		}
		else
		{
			//cout << "we have a regular chunk, no linking" << endl;
			ioTmpBuffer.clear();
			moveClusterToResult(ioCluster, ioResultStr, iUsedTypes);
			ioResultStr += iPreviousDelimiter;
			iPreviousDelimiter.clear();
		}
	}
	if(aLinked != LinkResult::invalid && aLinked != LinkResult::AND && aChunk->getValue() != NumberChunk::AND)
	{
		iPreviousChunk = aChunk;
		ioCluster.push_back(aChunk);
	}
	ioHasToLinkToPrevious = iCanLinkToNext && aLinked != LinkResult::invalid;
}

//This should probably be way better if implemented with a state machine, definitely not proud of that one
string NumberParser::parse(const SearchNode* iRootSn, const string& iText)
{
	if(iRootSn == nullptr || iText.empty())
	{
		return "";
	}

	const SearchNode* aSnWithValue = nullptr;		//This will tell us if we have found a Chunk
	const SearchNode* aCurrentSn = iRootSn;			//Current search node we are in

	bool aDelimiterFound = true;					//If its true, it means we can start looking for a chunk
	bool aLinkToPrevious = false;					//If its true, it means that next chunk might be linked to previous one
	NumberChunkPtr aPreviousChunk = nullptr;
	NumberChunkTypeMask aUsedTypes;					//See NumberChunk for more info on that one
	aUsedTypes.fill(false);
	vector<NumberChunkPtr> aCluster;				//We will store the chunks that form the same number here
	string aPreviousDelimiter = "";					//We need to keep this to not lose it while copying chars over

	string aTmpBuffer = "";							//We keep the characters we dont write (belonging to possible chunks) here
	string aResultStr = "";

	for(const char& aChar : iText)
	{
		////cout << aChar << ": [" << aPreviousDelimiter << "]" << endl;
		if(aDelimiterFound) 							//If char before was a delimiter, we can start looking for a number
		{
			aCurrentSn = aCurrentSn->get(tolower(aChar));
			if(aCurrentSn)								//We have found the start of a possible chunk
			{
				aTmpBuffer += aChar;
				if(aCurrentSn->getHasValue())
				{
					aSnWithValue = aCurrentSn;
				}
			}
			else										//Char does not belong to a chunk
			{
				aDelimiterFound = !isalpha(aChar);		//Store if char is a delimiter
				if(aSnWithValue != nullptr)				//This means we have chunk ready to be processed
				{
					if(aDelimiterFound)	//If CURRENT char is a delimiter we can process the chunk we found
					{
						processChunk(aSnWithValue, aLinkToPrevious, (aChar == ' '), aPreviousChunk, aPreviousDelimiter, aUsedTypes, aCluster, aResultStr, aTmpBuffer);
						aTmpBuffer += aChar;
						aPreviousDelimiter = aChar;
						//cout << "   " << aChar << ": [" << aPreviousDelimiter << "]" << endl;
					}
					else				//Discard current chunk, write previous cluster if
					{					//there was any, we know we wont be adding new things
						moveClusterToResult(aCluster, aResultStr, aUsedTypes);

						aResultStr += aTmpBuffer;
						aResultStr += aChar;
						aTmpBuffer.clear();
						aPreviousDelimiter.clear();
						aLinkToPrevious = false;
					}
					aSnWithValue = nullptr;
				}
				else					//This means we DONT have chunk ready to be processed, write previous cluster if
				{						//there was any, we know we wont be adding new things
					moveClusterToResult(aCluster, aResultStr, aUsedTypes);

					aResultStr += aTmpBuffer;
					aResultStr += aChar;
					aTmpBuffer.clear();
					aPreviousDelimiter.clear();
					aLinkToPrevious = false;
				}
				aCurrentSn = iRootSn;
			}
		}
		else
		{
			aResultStr += aChar;
			aDelimiterFound = !isalpha(aChar);
			aLinkToPrevious = false;
		}
	}

	if(aSnWithValue != nullptr)
	{
		processChunk(aSnWithValue, aLinkToPrevious, false/*linkToNext*/, aPreviousChunk, aPreviousDelimiter, aUsedTypes, aCluster, aResultStr, aTmpBuffer);
	}

	if(!aCluster.empty())
	{
		aResultStr += std::to_string(computeNumber(aCluster));
	}

	aResultStr += aTmpBuffer;
	return aResultStr;
}


