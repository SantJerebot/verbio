#include <fstream>
#include <string>
#include <iostream>
#include "NumberParser.h"
#include "SearchNode/SearchNode.h"

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		const std::string aFileName(argv[1]);
		if(argc > 2)
		{
			if(argv[2] == "years")
			{
				NumberChunk::PICK_UP_YEARS = true;
			}
			else
			{
				std::cerr << "Usage: " << argv[0] << " filename [years]" << std::endl;
				std::cerr << "  years: use this flag if you want to consider things like 'fifteen twenty' as a year ie(1520)" << std::endl;
				return 1;
			}
		}

		std::ifstream aFileStream;
		aFileStream.open(aFileName);
		if (aFileStream.fail()) {
			std::cerr << "File " << argv[1] << " does not exist" << std::endl;
			return 1;
		}
		std::string aText((std::istreambuf_iterator<char>(aFileStream)),
		                 std::istreambuf_iterator<char>());
		NumberParser aParser;
		std::shared_ptr<SearchNode> aSn = createSearchNodesForNumbers();


		std::ofstream aOutFile("replaced_" + aFileName);
		aOutFile << aParser.parse(aSn.get(), aText);
	}
	else
	{
		std::cerr << "Usage: " << argv[0] << " filename [years]" << std::endl;
		std::cerr << "  years: use this flag if you want to consider things like 'fifteen twenty' as a year ie(1520)" << std::endl;
		return 1;
	}
    return 0;
}
