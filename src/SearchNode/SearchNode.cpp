/*
 * SearchNode.cpp
 *
 *  Created on: 10 de jul. 2020
 *      Author: p.sivecas
 */
#include "SearchNode.h"
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

SearchNode::SearchNode(const std::vector<std::string>& iWords, const std::vector<NumberChunk>& iChunks): hasValue(false)
{
	if(iWords.size() == iChunks.size())
	{
		//TODO: Words can only contain alpha characters
		auto aChunkIt = iChunks.begin();
		for(auto& aWord: iWords)
		{
			this->addWord(aWord, *aChunkIt);
			aChunkIt++;
		}
	}
}

SearchNode::SearchNode(const string& iWord, const NumberChunk& iChunk): hasValue(false)
{
	this->addWord(iWord, iChunk);
}

SearchNode::SearchNode(string::const_iterator& iStrIt, const string& iWord, const NumberChunk& iChunk): hasValue(false)
{
	if(!iWord.empty())
	{
		if(iStrIt == (iWord.cend()-1))
		{
			hasValue = true;
			chunk = iChunk;
		}
		else
		{
			iStrIt++;
			char aChar = *iStrIt;
			children[aChar] = make_unique<SearchNode>(iStrIt, iWord, iChunk);
		}
	}
}

void SearchNode::addWord(const string& iWord, const NumberChunk& iChunk)
{
	if(!iWord.empty())
	{
		string::const_iterator aIt = iWord.cbegin();
		char aFirstChar = *aIt;
		if(children[aFirstChar] == nullptr)
		{
			children[aFirstChar] = make_unique<SearchNode>(aIt, iWord, iChunk);
		}
		else
		{
			children[aFirstChar]->addWord(aIt, iWord, iChunk);
		}
	}
}

void SearchNode::addWord(string::const_iterator& iStrIt, const string& iWord, const NumberChunk& iChunk)
{
	if(!iWord.empty())
	{
		if(iStrIt == (iWord.cend()-1))
		{
			hasValue = true;
			chunk = iChunk;
		}
		else
		{
			iStrIt++;
			char aChar = *iStrIt;
			if(children[aChar] == nullptr)
			{
				children[aChar] = make_unique<SearchNode>(iStrIt, iWord, iChunk);
			}
			else
			{
				children[aChar]->addWord(iStrIt, iWord, iChunk);
			}
		}
	}
}

bool SearchNode::getHasValue() const
{
	return hasValue;
}

const NumberChunk& SearchNode::getChunk() const
{
	return chunk;
}

const SearchNode* SearchNode::get(const char iCharIndex) const
{
	if(iCharIndex < 0 || iCharIndex >= ASCII_SIZE) return nullptr;
	return children[iCharIndex] == nullptr ? nullptr : children[iCharIndex].get();
}

const SearchNode* SearchNode::operator[](const char iCharIndex) const
{
	if(iCharIndex < 0 || iCharIndex >= ASCII_SIZE) return nullptr;
	return children[iCharIndex] == nullptr ? nullptr : children[iCharIndex].get();
}

shared_ptr<SearchNode> createSearchNodesForNumbers()
{
	vector<NumberChunk> aChunkTemplates =
	{
			NumberChunk(1, NumberChunkType::digit),
			NumberChunk(2, NumberChunkType::digit),
			NumberChunk(3, NumberChunkType::digit),
			NumberChunk(4, NumberChunkType::digit),
			NumberChunk(5, NumberChunkType::digit),
			NumberChunk(6, NumberChunkType::digit),
			NumberChunk(7, NumberChunkType::digit),
			NumberChunk(8, NumberChunkType::digit),
			NumberChunk(9, NumberChunkType::digit),
			NumberChunk(10, NumberChunkType::two_digits),
			NumberChunk(11, NumberChunkType::two_digits),
			NumberChunk(12, NumberChunkType::two_digits),
			NumberChunk(13, NumberChunkType::two_digits),
			NumberChunk(14, NumberChunkType::two_digits),
			NumberChunk(15, NumberChunkType::two_digits),
			NumberChunk(16, NumberChunkType::two_digits),
			NumberChunk(17, NumberChunkType::two_digits),
			NumberChunk(18, NumberChunkType::two_digits),
			NumberChunk(19, NumberChunkType::two_digits)
	};

	for(int i=20; i<100; i++)
	{
		aChunkTemplates.push_back(NumberChunk(i, NumberChunkType::two_digits));
	}


	aChunkTemplates.push_back(NumberChunk(100, NumberChunkType::hundred));
	aChunkTemplates.push_back(NumberChunk(1000, NumberChunkType::thousand));
	aChunkTemplates.push_back(NumberChunk(1000000, NumberChunkType::million));
	aChunkTemplates.push_back(NumberChunk(1000000000, NumberChunkType::billion));
	aChunkTemplates.push_back(NumberChunk(NumberChunk::AND, NumberChunkType::invalid));

	vector<std::string> aWords =
	{
			"one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
			"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
			"twenty",
			"twenty-one",
			"twenty-two",
			"twenty-three",
			"twenty-four",
			"twenty-five",
			"twenty-six",
			"twenty-seven",
			"twenty-eight",
			"twenty-nine",
			"thirty",
			"thirty-one",
			"thirty-two",
			"thirty-three",
			"thirty-four",
			"thirty-five",
			"thirty-six",
			"thirty-seven",
			"thirty-eight",
			"thirty-nine",
			"forty",
			"forty-one",
			"forty-two",
			"forty-three",
			"forty-four",
			"forty-five",
			"forty-six",
			"forty-seven",
			"forty-eight",
			"forty-nine",
			"fifty",
			"fifty-one",
			"fifty-two",
			"fifty-three",
			"fifty-four",
			"fifty-five",
			"fifty-six",
			"fifty-seven",
			"fifty-eight",
			"fifty-nine",
			"sixty",
			"sixty-one",
			"sixty-two",
			"sixty-three",
			"sixty-four",
			"sixty-five",
			"sixty-six",
			"sixty-seven",
			"sixty-eight",
			"sixty-nine",
			"seventy",
			"seventy-one",
			"seventy-two",
			"seventy-three",
			"seventy-four",
			"seventy-five",
			"seventy-six",
			"seventy-seven",
			"seventy-eight",
			"seventy-nine",
			"eighty",
			"eighty-one",
			"eighty-two",
			"eighty-three",
			"eighty-four",
			"eighty-five",
			"eighty-six",
			"eighty-seven",
			"eighty-eight",
			"eighty-nine",
			"ninety",
			"ninety-one",
			"ninety-two",
			"ninety-three",
			"ninety-four",
			"ninety-five",
			"ninety-six",
			"ninety-seven",
			"ninety-eight",
			"ninety-nine",
			"hundred", "thousand", "million", "billion", "and"
	};
	return make_shared<SearchNode>(aWords, aChunkTemplates);
}
