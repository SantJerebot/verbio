/*
 * SearchNode.h
 *
 *  Created on: 10 de jul. 2020
 *      Author: p.sivecas
 */
#pragma once

#include <string>
#include <array>
#include <vector>
#include <memory>
#include <iostream>

#include "NumberChunk.h"

/*
 * This class is used to search for words. Each node contains one or more of the characters we are looking for
 * in an array of pointers to the next nodes. The pointers are indexed using the characters themselves, for
 * increased efficiency. If a character is not a match, we return a nullptr.
 *
 * Notice that this index limits the search to only ascii characters. Should the need to search for UTF-8 words
 * arise, the structure should change to an unordered_map.
 *
 * The only node accessed from the outside is the root node, which is created by a free function called
 * createSearchNodesForNumbers (this should not be in this class, but time has been an issue).
 *
 * Other nodes are stored using a unique_ptr as this class is the sole owner of its structure.
 *
 * When a node is found, a NumberChunk instance is returned, the caller can use this instances to link them
 * in order to create clusters. This should be a generic chunk of course, but due to time constraints this will have to do for now.
 */

class SearchNode
{
    private:
		static constexpr size_t ASCII_SIZE = 128;
		bool hasValue;
		std::array<std::unique_ptr<SearchNode>, ASCII_SIZE> children;
		NumberChunk chunk;

    public:
		SearchNode(const std::vector<std::string>& iWords, const std::vector<NumberChunk>& iChunks);
		SearchNode(const std::string& iWord, const NumberChunk& iChunk);
		SearchNode(std::string::const_iterator& iStrIt, const std::string& iWord, const NumberChunk& iChunk);
		void addWord(const std::string& iWord, const NumberChunk& iChunk );
		void addWord(std::string::const_iterator& iStrIt, const std::string& iWord, const NumberChunk& iChunk);
		const NumberChunk& getChunk() const;
		bool getHasValue() const;
		const SearchNode* get(const char iCharIndex) const;
		const SearchNode* operator[](const char iCharIndex) const;
};

std::shared_ptr<SearchNode> createSearchNodesForNumbers();
