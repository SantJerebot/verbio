/*
 * Node-test.cpp
 *
 *  Created on: 8 de jul. 2020
 *      Author: p.sivecas
 */
#include "gtest/gtest.h"
#include "NumberParser.h"
#include "SearchNode/SearchNode.h"

class NumberParserTest : public ::testing::Test {

  protected:
	std::shared_ptr<SearchNode> sn;
	NumberParser numParser;

	NumberParserTest() {
		//auto old_buffer = std::cout.rdbuf(nullptr);
    }

    virtual ~NumberParserTest() {
    }

    virtual void SetUp() {
    	sn = createSearchNodesForNumbers();
    }

    virtual void TearDown() {
    }

  };

TEST_F(NumberParserTest, Simple)
{
	const std::string aResult = numParser.parse(sn.get(), "one ");
	EXPECT_EQ(aResult, "1 ");
}

TEST_F(NumberParserTest, TwoNumbers)
{
	const std::string aResult = numParser.parse(sn.get(), "one two");
	EXPECT_EQ(aResult, "1 2");
}

TEST_F(NumberParserTest, TwoNumbersNoDelimiter)
{
	const std::string aResult = numParser.parse(sn.get(), "onetwo");
	EXPECT_EQ(aResult, "onetwo");
}

TEST_F(NumberParserTest, TwoNumbersSeparatedBySpace)
{
	const std::string aResult = numParser.parse(sn.get(), "I like the number one hundred thousand hundred thousand twenty-one, yes I do!");
	EXPECT_EQ(aResult, "I like the number 100100 1021, yes I do!");
}

TEST_F(NumberParserTest, StrangeDelimiters)
{
	const std::string aResult = numParser.parse(sn.get(), "I like the number one hundred+thousand hundred&thousand twenty one, I do it times(twenty)");
	EXPECT_EQ(aResult, "I like the number 100+1100&1020 1, I do it times(20)");
}

TEST_F(NumberParserTest, NoNumber)
{
	const std::string aResult = numParser.parse(sn.get(), "I like numbers, just not writing them!");
	EXPECT_EQ(aResult, "I like numbers, just not writing them!");
}

TEST_F(NumberParserTest, EmptyString)
{
	const std::string aResult = numParser.parse(sn.get(), "");
	EXPECT_EQ(aResult, "");
}

TEST_F(NumberParserTest, Fractions)
{
	const std::string aResult = numParser.parse(sn.get(), "Fractions are easy: four/five for example is 4/5.");
	EXPECT_EQ(aResult, "Fractions are easy: 4/5 for example is 4/5.");
}

TEST_F(NumberParserTest, SimpleNumber)
{
	const std::string aResult = numParser.parse(sn.get(), "thousand hundred forty-three");
	EXPECT_EQ(aResult, "1143");
}

TEST_F(NumberParserTest, DoubleSpacesSimple)
{
	const std::string aResult = numParser.parse(sn.get(), "forty  three is not the same as thousand hundred forty-three");
	EXPECT_EQ(aResult, "40  3 is not the same as 1143");
}

TEST_F(NumberParserTest, DoubleSpaces)
{
	const std::string aResult = numParser.parse(sn.get(), "thousand hundred  three and thousand hundred");
	EXPECT_EQ(aResult, "1100  3 and 1100");
}

TEST_F(NumberParserTest, SameNumberTwice)
{
	const std::string aResult = numParser.parse(sn.get(), "hundred hundred");
	EXPECT_EQ(aResult, "100 100");
}

TEST_F(NumberParserTest, MixedCase)
{
	const std::string aResult = numParser.parse(sn.get(), "HunDred hundRED");
	EXPECT_EQ(aResult, "100 100");
}

TEST_F(NumberParserTest, Number_AND_NonSpaceDelimiter)
{
	const std::string aResult = numParser.parse(sn.get(), "hundred and+");
	EXPECT_EQ(aResult, "100 and+");
}

TEST_F(NumberParserTest, NonLinkableNumber_AND_SpaceDelimiter)
{
	const std::string aResult = numParser.parse(sn.get(), "twenty and three");
	EXPECT_EQ(aResult, "20 and 3");
}

TEST_F(NumberParserTest, NonLinkableNumber_AND_NonSpaceDelimiter)
{
	const std::string aResult = numParser.parse(sn.get(), "twenty and+three");
	EXPECT_EQ(aResult, "20 and+3");
}

TEST_F(NumberParserTest, NonLinkableNumber_AND_OtherChars)
{
	const std::string aResult = numParser.parse(sn.get(), "twenty and twerp");
	EXPECT_EQ(aResult, "20 and twerp");
}

TEST_F(NumberParserTest, ANDSimple)
{
	const std::string aResult = numParser.parse(sn.get(), "hundred and twenty-three");
	EXPECT_EQ(aResult, "123");
}

TEST_F(NumberParserTest, LinkableNumber_AND_OtherChars)
{
	const std::string aResult = numParser.parse(sn.get(), "hundred and twerp");
	EXPECT_EQ(aResult, "100 and twerp");
}

TEST_F(NumberParserTest, Combinations_with_AND)
{
	const std::string aResult = numParser.parse(sn.get(), "I like one and hundred thousand and twenty-three and hundred five");
	EXPECT_EQ(aResult, "I like 1 and 100023 and 105");
}

TEST_F(NumberParserTest, AND_when_next_is_bigger)
{
	const std::string aResult = numParser.parse(sn.get(), "hundred and thousand");
	EXPECT_EQ(aResult, "100 and 1000");
}

TEST_F(NumberParserTest, TwoTys)
{
	const std::string aResult = numParser.parse(sn.get(), "twenty-three forty-four");
	EXPECT_EQ(aResult, "23 44");
}

TEST_F(NumberParserTest, TyAndDigit)
{
	const std::string aResult = numParser.parse(sn.get(), "twenty three forty-four");
	EXPECT_EQ(aResult, "20 3 44");
}

TEST_F(NumberParserTest, TeenHundred)
{
	const std::string aResult = numParser.parse(sn.get(), "fifteen hundred");
	EXPECT_EQ(aResult, "1500");
}

TEST_F(NumberParserTest, TeenHundredThousand)
{
	const std::string aResult = numParser.parse(sn.get(), "fifteen hundred thousand");
	EXPECT_EQ(aResult, "1500 1000");
}

TEST_F(NumberParserTest, SimpleYear)
{
	NumberChunk::PICK_UP_YEARS = true;
	std::string aResult = numParser.parse(sn.get(), "She was born in fifteen seventy-five, it was raining.");
	EXPECT_EQ(aResult, "She was born in 1575, it was raining.");
	NumberChunk::PICK_UP_YEARS = false;
	aResult = numParser.parse(sn.get(), "She was born in fifteen seventy-five, it was raining.");
	EXPECT_EQ(aResult, "She was born in 15 75, it was raining.");
}

TEST_F(NumberParserTest, YearAndMoreNumbers)
{
	NumberChunk::PICK_UP_YEARS = true;
	std::string aResult = numParser.parse(sn.get(), "She was born in fifteen seventy-five million thousand, it was raining.");
	EXPECT_EQ(aResult, "She was born in 1575 1001000, it was raining.");
	NumberChunk::PICK_UP_YEARS = false;
	aResult = numParser.parse(sn.get(), "She was born in fifteen seventy-five million thousand, it was raining.");
	EXPECT_EQ(aResult, "She was born in 15 75001000, it was raining.");
}

TEST_F(NumberParserTest, OneMillionBillion)
{
	std::string aResult = numParser.parse(sn.get(), "One million billion");
	EXPECT_EQ(aResult, "1000000 1000000000");
}

TEST_F(NumberParserTest, OneBillion)
{
	std::string aResult = numParser.parse(sn.get(), "One billion");
	EXPECT_EQ(aResult, "1000000000");
}

TEST_F(NumberParserTest, HundredOneBillion)
{
	std::string aResult = numParser.parse(sn.get(), "a Hundred and One billion");
	EXPECT_EQ(aResult, "a 101 1000000000");
}

TEST_F(NumberParserTest, AND_alone)
{
	std::string aResult = numParser.parse(sn.get(), " and ");
	EXPECT_EQ(aResult, " and ");
}

TEST_F(NumberParserTest, AND_with_two)
{
	std::string aResult = numParser.parse(sn.get(), "and 2");
	EXPECT_EQ(aResult, "and 2");
}
