/*
 * Node-test.cpp
 *
 *  Created on: 8 de jul. 2020
 *      Author: p.sivecas
 */
#include "gtest/gtest.h"
#include "NumberChunk.h"
#include <tuple>


TEST(NumberChunk, Initialization)
{
	NumberChunk aNc1(1, NumberChunkType::digit);
	NumberChunk aNc2(2, NumberChunkType::digit);
	EXPECT_EQ(aNc1.getValue(), 1);
	EXPECT_EQ(aNc2.getValue(), 2);
	EXPECT_EQ(aNc1.getType(), NumberChunkType::digit);
	EXPECT_EQ(aNc2.getType(), NumberChunkType::digit);
}

class Dummies{
public:
	static NumberChunkPtr getDigit1() {return std::make_shared<NumberChunk>(1, NumberChunkType::digit);}
	static NumberChunkPtr getDigit2() {return std::make_shared<NumberChunk>(2, NumberChunkType::digit);}

	static NumberChunkPtr getTeen1() {return std::make_shared<NumberChunk>(11, NumberChunkType::two_digits);}
	static NumberChunkPtr getTeen2() {return std::make_shared<NumberChunk>(12, NumberChunkType::two_digits);}

	static NumberChunkPtr getTy1() {return std::make_shared<NumberChunk>(20, NumberChunkType::two_digits);}
	static NumberChunkPtr getTy2() {return std::make_shared<NumberChunk>(30, NumberChunkType::two_digits);}

	static NumberChunkPtr getHundred() {return std::make_shared<NumberChunk>(100, NumberChunkType::hundred);}
	static NumberChunkPtr getThousand() {return std::make_shared<NumberChunk>(1000, NumberChunkType::thousand);}
	static NumberChunkPtr getMillion() {return std::make_shared<NumberChunk>(1000000, NumberChunkType::million);}
	static NumberChunkPtr getBillion() {return std::make_shared<NumberChunk>(1000000, NumberChunkType::billion);}
};

class CanNotLinkCurrentToNullTest :public ::testing::TestWithParam<NumberChunkPtr> {
protected:
	NumberChunkPtr aNc1 = nullptr;
};

TEST_P(CanNotLinkCurrentToNullTest, NormalCases) {
	NumberChunkPtr aCurrentNc = GetParam();
	NumberChunkPtr aPreviousNc = nullptr;
	NumberChunkTypeMask aMask;
	aMask.fill(false);
	LinkResult aLr = linkCurrentToPreviousChunk(aCurrentNc, aPreviousNc, aMask);
	EXPECT_EQ(aLr, LinkResult::standalone);
}

INSTANTIATE_TEST_CASE_P(
		NumberChunk,
		CanNotLinkCurrentToNullTest,
        ::testing::Values(
        		Dummies::getDigit2(),
				Dummies::getTeen2(),
				Dummies::getTy2(),
				Dummies::getHundred(),
				Dummies::getThousand(),
				Dummies::getMillion(),
				Dummies::getBillion()
        ));

class CanLinkToPreviousTest :public ::testing::TestWithParam< std::tuple<NumberChunkPtr, NumberChunkPtr>> {};

TEST_P(CanLinkToPreviousTest, NormalCases) {
	NumberChunkPtr aCurrentNc = std::get<0>(GetParam());
	NumberChunkPtr aPreviousNc = std::get<1>(GetParam());
	NumberChunkTypeMask aMask;
	aMask.fill(false);
	aMask[static_cast<unsigned int>(aPreviousNc->getType())] = true;
	LinkResult aLr = linkCurrentToPreviousChunk(aCurrentNc, aPreviousNc, aMask);
	EXPECT_EQ(aLr, LinkResult::linked);
	EXPECT_EQ(aPreviousNc->getNextChunk(), aCurrentNc);
}

INSTANTIATE_TEST_CASE_P(
		NumberChunk,
		CanLinkToPreviousTest,
        ::testing::Values(
        		//Digit to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getHundred()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getThousand()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getMillion()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getBillion()),
				//Teen to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTeen2(), Dummies::getHundred()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTeen2(), Dummies::getThousand()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTeen2(), Dummies::getMillion()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTeen2(), Dummies::getBillion()),
				//Ty to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTy2(), Dummies::getHundred()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTy2(), Dummies::getThousand()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTy2(), Dummies::getMillion()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTy2(), Dummies::getBillion()),
				//Hundred to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getHundred(), Dummies::getDigit1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getHundred(), Dummies::getTeen1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getHundred(), Dummies::getTy1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getHundred(), Dummies::getThousand()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getHundred(), Dummies::getMillion()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getHundred(), Dummies::getBillion()),
				//Thousand to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getThousand(), Dummies::getDigit1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getThousand(), Dummies::getTeen1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getThousand(), Dummies::getTy1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getThousand(), Dummies::getHundred()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getThousand(), Dummies::getMillion()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getThousand(), Dummies::getBillion()),
				//Million to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getMillion(), Dummies::getDigit1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getMillion(), Dummies::getTeen1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getMillion(), Dummies::getTy1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getMillion(), Dummies::getHundred()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getMillion(), Dummies::getBillion()),
				//One Billion (That is the maximum we can reach)
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getDigit1())

        ));

class CanNotLinkToPreviousTest :public ::testing::TestWithParam< std::tuple<NumberChunkPtr, NumberChunkPtr>> {};

TEST_P(CanNotLinkToPreviousTest, NormalCases) {
	NumberChunkPtr aCurrentNc = std::get<0>(GetParam());
	NumberChunkPtr aPreviousNc = std::get<1>(GetParam());
	NumberChunkTypeMask aMask;
	aMask.fill(false);
	aMask[static_cast<unsigned int>(aPreviousNc->getType())] = true;
	LinkResult aLr = linkCurrentToPreviousChunk(aCurrentNc, aPreviousNc, aMask);
	EXPECT_EQ(aLr, LinkResult::standalone);
	EXPECT_EQ(aPreviousNc->getNextChunk(), nullptr);
}

INSTANTIATE_TEST_CASE_P(
		NumberChunk,
		CanNotLinkToPreviousTest,
        ::testing::Values(
        		//Digit to previous
        		std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getDigit1()),
        		std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getDigit1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getTeen1()),
				//Teen to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTeen2(), Dummies::getDigit1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTeen2(), Dummies::getTeen1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTeen2(), Dummies::getTy1()),
				//Ty to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getDigit2(), Dummies::getTy1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTy2(), Dummies::getTeen1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getTy2(), Dummies::getTy1()),
				//Hundred to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getHundred(), Dummies::getHundred()),
				//Thousand to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getThousand(), Dummies::getThousand()),
				//Million to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getMillion(), Dummies::getThousand()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getMillion(), Dummies::getMillion()),
				//Billion to previous
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getBillion()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getDigit2()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getTeen1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getTy1()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getHundred()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getThousand()),
				std::tuple<NumberChunkPtr, NumberChunkPtr>(Dummies::getBillion(), Dummies::getMillion())
        ));

TEST(NumberChunk, CanNotLinkToPreviousComplexTest)
{
	NumberChunkPtr aNc1 = Dummies::getHundred();
	NumberChunkPtr aNc2 = Dummies::getThousand();
	NumberChunkPtr aNc3 = Dummies::getHundred();
	NumberChunkPtr aNc4 = Dummies::getThousand();
	NumberChunkTypeMask aMask;
	aMask.fill(false);
	aMask[static_cast<unsigned int>(aNc1->getType())] = true;
	LinkResult aLr1_2 = linkCurrentToPreviousChunk(aNc2, aNc1, aMask);
	EXPECT_EQ(aLr1_2, LinkResult::linked);
	EXPECT_EQ(aNc1->getNextChunk(), aNc2);
	LinkResult aLr2_3 = linkCurrentToPreviousChunk(aNc3, aNc2, aMask);
	EXPECT_EQ(aLr2_3, LinkResult::linked);
	EXPECT_EQ(aNc2->getNextChunk(), aNc3);
	LinkResult aLr3_4 = linkCurrentToPreviousChunk(aNc4, aNc3, aMask);
	EXPECT_EQ(aLr3_4, LinkResult::standalone);
	EXPECT_EQ(aNc3->getNextChunk(), nullptr);
}

TEST(NumberChunk, CanNotLinkToPreviousComplex2Test)
{
	NumberChunkPtr aNc1 = Dummies::getThousand();
	NumberChunkPtr aNc2 = Dummies::getBillion();
	NumberChunkPtr aNc3 = Dummies::getMillion();
	NumberChunkPtr aNc4 = Dummies::getDigit1();
	NumberChunkPtr aNc5 = Dummies::getHundred();
	NumberChunkPtr aNc6 = Dummies::getTy1();
	NumberChunkPtr aNc7 = Dummies::getMillion();
	NumberChunkTypeMask aMask;
	aMask.fill(false);
	aMask[static_cast<unsigned int>(aNc1->getType())] = true;
	LinkResult aLr1_2 = linkCurrentToPreviousChunk(aNc2, aNc1, aMask);
	EXPECT_EQ(aLr1_2, LinkResult::standalone);
	EXPECT_EQ(aNc1->getNextChunk(), nullptr);
	LinkResult aLr2_3 = linkCurrentToPreviousChunk(aNc3, aNc2, aMask);
	EXPECT_EQ(aLr2_3, LinkResult::linked);
	EXPECT_EQ(aNc2->getNextChunk(), aNc3);
	LinkResult aLr3_4 = linkCurrentToPreviousChunk(aNc4, aNc3, aMask);
	EXPECT_EQ(aLr3_4, LinkResult::linked);
	EXPECT_EQ(aNc3->getNextChunk(), aNc4);
	LinkResult aLr4_5 = linkCurrentToPreviousChunk(aNc5, aNc4, aMask);
	EXPECT_EQ(aLr4_5, LinkResult::linked);
	EXPECT_EQ(aNc4->getNextChunk(), aNc5);
	LinkResult aLr5_6 = linkCurrentToPreviousChunk(aNc6, aNc5, aMask);
	EXPECT_EQ(aLr5_6, LinkResult::linked);
	EXPECT_EQ(aNc5->getNextChunk(), aNc6);
	LinkResult aLr6_7 = linkCurrentToPreviousChunk(aNc7, aNc6, aMask);
	EXPECT_EQ(aLr6_7, LinkResult::standalone);
	EXPECT_EQ(aNc6->getNextChunk(), nullptr);
}

TEST(NumberChunk, ComputeNumber)
{
	std::vector<NumberChunkPtr> aCluster = {
				Dummies::getDigit1(),
				Dummies::getMillion(),
				Dummies::getDigit2(),
				Dummies::getHundred(),
				Dummies::getTy1(),
				Dummies::getDigit1(),
				Dummies::getThousand()
	};
	EXPECT_EQ(computeNumber(aCluster), 1221000);
}

TEST(NumberChunk, ComputeNumber2)
{
	std::vector<NumberChunkPtr> aCluster = {
				Dummies::getDigit1(),
				Dummies::getHundred(),
				Dummies::getThousand(),
				Dummies::getHundred()
	};
	EXPECT_EQ(computeNumber(aCluster), 100100);
}
