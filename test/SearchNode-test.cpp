#include "gtest/gtest.h"
#include "SearchNode/SearchNode.h"

TEST(SearchNode, Initialize)
{
	std::cout << (int)'�' << std::endl;
	NumberChunk aOneChunk(1, NumberChunkType::digit);
	SearchNode aSn("one", aOneChunk);
	EXPECT_EQ(aSn['b'], nullptr);
	EXPECT_EQ(aSn['�'], nullptr);
	EXPECT_NE(aSn['o'], nullptr);
	EXPECT_NE(aSn['o']->get('n'), nullptr);
	EXPECT_EQ(aSn['o']->get('n')->getChunk().getValue(), 0);
	EXPECT_EQ(aSn['o']->get('n')->getChunk().getType(), NumberChunkType::invalid);
	EXPECT_NE(aSn['o']->get('n')->get('e'), nullptr);
	EXPECT_EQ(aSn['o']->get('n')->get('e')->getChunk().getValue(), 1);
}

TEST(SearchNode, AddWord)
{
	NumberChunk aOneChunk(1, NumberChunkType::digit);
	NumberChunk aTwoChunk(2, NumberChunkType::digit);
	NumberChunk aTChunk(121, NumberChunkType::invalid);
	NumberChunk aTwerpChunk(122, NumberChunkType::invalid);
	SearchNode aSn("one", aOneChunk);
	aSn.addWord("two", aTwoChunk);
	EXPECT_EQ(aSn['t']->get('w')->get('o')->getChunk().getValue(), 2);
	EXPECT_FALSE(aSn['t']->getHasValue());
	aSn.addWord("t", aTChunk);
	EXPECT_TRUE(aSn['t']->getHasValue());
	EXPECT_EQ(aSn['t']->getChunk().getValue(), 121);
	aSn.addWord("twerp", aTwerpChunk);
	EXPECT_TRUE(aSn['t']->get('w')->get('e')->get('r')->get('p')->getHasValue());
	EXPECT_EQ(aSn['t']->get('w')->get('e')->get('r')->get('p')->getChunk().getValue(), 122);
}

TEST(SearchNode, AddWords)
{
	std::vector<std::string> aWords = {"one", "two", "three"};
	std::vector<NumberChunk> aChunks = {NumberChunk(1,NumberChunkType::digit),
			NumberChunk(2, NumberChunkType::digit),
			NumberChunk(3, NumberChunkType::digit)};
	SearchNode aSn(aWords, aChunks);
	EXPECT_EQ(aSn['o']->get('n')->get('e')->getChunk().getValue(), 1);
	EXPECT_EQ(aSn['t']->get('w')->get('o')->getChunk().getValue(), 2);
	EXPECT_EQ(aSn['t']->get('h')->get('r')->get('e')->get('e')->getChunk().getValue(), 3);
}
